package org.potatohed.starch.controller

import org.neo4j.ogm.session.Session
import org.potatohed.starch.objects.neo4j.ApplicationNode
import org.potatohed.starch.objects.neo4j.Relationship
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("neo4j")
class NeoController(val session: Session) {

    @PostMapping("/query")
    fun postQuery(@RequestBody query: String) = session.query(query, emptyMap<String, Any>()).queryResults()

    @GetMapping("/applications")
    fun getNodes() = session.loadAll(ApplicationNode::class.java)

    @GetMapping("/rels")
    fun getRelationships() = session.loadAll(Relationship::class.java)
}