package org.potatohed.starch

import org.neo4j.ogm.config.Configuration
import org.neo4j.ogm.session.SessionFactory
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer
import org.springframework.core.io.support.ResourcePatternResolver
import org.springframework.data.neo4j.transaction.Neo4jTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement



@SpringBootApplication
@EnableTransactionManagement
class StarchApplication {

    @Bean
    fun sessionFactory(neo4jConfig: org.neo4j.ogm.config.Configuration): SessionFactory {
        return SessionFactory(neo4jConfig, "org.potatohed.starch.objects.neo4j")
    }

    @Bean
    fun configuration(): org.neo4j.ogm.config.Configuration {
        return Configuration.Builder()
                .build()
    }

    @Bean
    fun transactionManager(sessionFactory: SessionFactory): Neo4jTransactionManager {
        return Neo4jTransactionManager(sessionFactory)
    }


    @Bean
    fun yamlProperties(resolver: ResourcePatternResolver): PropertySourcesPlaceholderConfigurer {
        val propertySourcesPlaceholderConfigurer = PropertySourcesPlaceholderConfigurer()
        val yaml = YamlPropertiesFactoryBean()
        yaml.setResources(*resolver.getResources("classpath:/objects/*.yml"),
            *resolver.getResources("file:objects/*.yml") )
        propertySourcesPlaceholderConfigurer.setProperties(yaml.`object`)
        return propertySourcesPlaceholderConfigurer
    }

}

fun main(args: Array<String>) {
    SpringApplication.run(StarchApplication::class.java, *args)
}
