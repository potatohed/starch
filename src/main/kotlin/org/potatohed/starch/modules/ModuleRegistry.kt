package org.potatohed.starch.modules

import com.google.common.collect.LinkedListMultimap
import com.google.common.collect.Multimap
import org.potatohed.starch.controller.Node
import org.potatohed.starch.objects.Entity
import org.springframework.stereotype.Component

@Component
class ModuleRegistry {

    val modules: Multimap<InvocationPredicate, EntityModule> = LinkedListMultimap.create()

    public fun registerModule(module: EntityModule, invocationPredicate: InvocationPredicate) {
        modules.put(invocationPredicate, module)
    }
}

interface EntityModule {

    /**
     * Trigger an update to the state. This should be non blocking, and resolve in 10ms or less
     */
    public fun update(entity: Entity)

    /**
     * Return the current state for the Entity. This operation may block.
     */
    public fun state(entity: Entity): List<ModuleState>

    val moduleId: String

}

interface ModuleState {
    /**
     * Current state of the application as provided by the module
     * GOOD: Application is healthy
     * UNHEALTHY: Application is unhealthy, but possibly still functional. (Degraded)
     * BAD: Application is unhealthy, and is not functional. (Down)
     * UNKNOWN: Unable to obtain the status. (Module cannot reach a middle man service. Ex Unable to connect to the NGINX API)
     * NOT_APPLICABLE: The entity does not have sufficient configuration to utilize this module.
     */
    enum class STATUS(val severity: Int) { GOOD(0), UNKNOWN(0), UNHEALTHY(0),  BAD(0) }

    val status: STATUS

    /**
     * Displayable string representing the module
     */
    val label: String

    val properties: Map<String, String>

    fun toNode(): Node {
        return Node(name = this.label,
            color = when (this.status) {
                STATUS.GOOD -> "#008000"
                STATUS.BAD -> "#FF0000"
                else -> "#FFFF00"
            },
            children = emptyList())
    }
}

class SimpleModuleState(
    override val status: ModuleState.STATUS,
    override val label: String,
    override val properties: Map<String, String> = emptyMap()) : ModuleState

interface InvocationPredicate {
    /**
     * Allowed check frequencies.
     * SECOND: Should be used for most modules.
     * HALF_SECOND: Used for modules who need to check with better resolution.
     * TEN_MILLI: Should only be used when absolutely needed.
     */
    enum class Frequency { TEN_MILLI, HALF_SECOND, SECOND }

    /**
     * Frequency at which shouldInvoke should be checked.
     * See {@code Frequency}
     */
    fun frequency(): Frequency

    fun shouldInvoke(): Boolean
}
