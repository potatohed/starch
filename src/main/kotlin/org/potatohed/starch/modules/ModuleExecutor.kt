package org.potatohed.starch.modules

import com.sun.management.OperatingSystemMXBean
import org.potatohed.starch.objects.ApplicationEntity
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.lang.management.ManagementFactory
import javax.management.MBeanServerConnection

val USAGELOGGER = LoggerFactory.getLogger("cpu.usage")


@Service
class ModuleService(val registry: ModuleRegistry, val applicationEntity: List<ApplicationEntity>) {

    var mbsc: MBeanServerConnection = ManagementFactory.getPlatformMBeanServer()

    var osMBean: OperatingSystemMXBean = ManagementFactory.newPlatformMXBeanProxy(
            mbsc, ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME, OperatingSystemMXBean::class.java)

    @Scheduled(fixedRate = 5000L, initialDelay = 5000L)
    fun usage() {
        USAGELOGGER.info("CPU LOAD: %.2f%% ".format(osMBean.processCpuLoad * 100))
    }

    @Scheduled(fixedRate = 1000L)
    fun evaluateSecondModules() {
        registry.modules.entries()
                .stream().parallel()
                .filter({ it.key.frequency() == InvocationPredicate.Frequency.SECOND })
                .filter({ it.key.shouldInvoke() })
                .map { it.value }
                .forEach { module -> applicationEntity.forEach { entity -> suppress { module.update(entity) } } }
    }

    @Scheduled(fixedRate = 500L)
    fun evaluateHalfSecondModules() {
        registry.modules.entries()
                .stream().parallel()
                .filter({ it.key.frequency() == InvocationPredicate.Frequency.HALF_SECOND })
                .filter({ it.key.shouldInvoke() })
                .map { it.value }
                .forEach { module -> applicationEntity.forEach { entity -> suppress { module.update(entity) } } }
    }

    @Scheduled(fixedRate = 10L)
    fun evaluateTenMilliModules() {
        registry.modules.entries()
                .stream().parallel()
                .filter({ it.key.frequency() == InvocationPredicate.Frequency.TEN_MILLI })
                .filter({ it.key.shouldInvoke() })
                .map { it.value }
                .forEach { module -> applicationEntity.forEach { entity -> suppress { module.update(entity) } } }
    }


    private fun suppress(function: () -> Unit) {
        try {
            function()
        } catch (e: Exception) {
            println(e)
            //OPPRESS, I MEAN SUPPRESS!!!!
        }
    }

}