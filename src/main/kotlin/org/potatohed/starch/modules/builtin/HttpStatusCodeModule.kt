package org.potatohed.starch.modules.builtin

import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.newFixedThreadPoolContext
import org.apache.commons.lang3.StringUtils.EMPTY
import org.potatohed.starch.modules.EntityModule
import org.potatohed.starch.modules.ModuleRegistry
import org.potatohed.starch.modules.ModuleState
import org.potatohed.starch.modules.SimpleModuleState
import org.potatohed.starch.objects.ApplicationEntity
import org.potatohed.starch.objects.Entity
import org.potatohed.starch.util.noSSLValidationRequestFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.http.client.ClientHttpResponse
import org.springframework.stereotype.Component
import org.springframework.web.client.ResponseErrorHandler
import org.springframework.web.client.RestClientException
import javax.annotation.PostConstruct

const val CONFIG_NAME = "httpHealthcheck"
const val URLS = "urls"
const val EXPECTED_CODE = "expectedCode"

@Component
class HttpStatusCodeModule : EntityModule {

    val threadPool = newFixedThreadPoolContext(16, "HTTP-ASYNC")
    val statusCodeRegex = "^[1-5][0-9][0-9]\$".toRegex()

    class Configuration(private val internalMap: Map<String, Any>)

    override val moduleId: String
        get() = CONFIG_NAME

    val states: MutableMap<String, MutableMap<String, ModuleState>> = HashMap()

    val restTemplate = RestTemplateBuilder()
            .errorHandler(NoExceptionErrorHandler())
            .requestFactory(noSSLValidationRequestFactory)
            .build()!!

    override fun update(entity: Entity) {
        if (!isConfigured(entity) && entity !is ApplicationEntity) {
            return
        }
        val urls = extractHealthChecks(entity as ApplicationEntity)
        val statusCode = expctedStatusCode(entity)
        runHealthChecks(urls, statusCode, entity.name, entity.appId)
    }

    private fun runHealthChecks(urls: List<String>, statusCode: HttpStatus, name: String, appId: String) {
        urls.forEach {
            states.putIfAbsent(appId, HashMap())
            launch(threadPool) {
                states.getValue(appId)[it] = doHttpCheck(it, httpStatus = statusCode, name = name)
            }
        }
    }

    private fun extractHealthChecks(applicationEntity: ApplicationEntity): List<String> {
        val urls: Any = applicationEntity.configuration[CONFIG_NAME]?.get(URLS) ?: emptyList<String>()
        return when (urls) {
            is List<*> -> urls.filter { it is String }.map { it as String }
            is String -> listOf(urls)
            else -> emptyList()
        }
    }

    private fun expctedStatusCode(applicationEntity: ApplicationEntity): HttpStatus {
        val statusCode: Any = applicationEntity.configuration[CONFIG_NAME]?.get(EXPECTED_CODE) ?: HttpStatus.OK
        return when (statusCode) {
            is Int -> HttpStatus.valueOf(statusCode)
            is String -> when {
                parsable(statusCode) -> HttpStatus.valueOf(Integer.parseInt(statusCode))
                else -> HttpStatus.valueOf(statusCode.toUpperCase())
            }
            else -> HttpStatus.OK
        }
    }

    private fun parsable(statusCode: String): Boolean {
        return statusCode.matches(statusCodeRegex)
    }

    private fun doHttpCheck(url: String, httpStatus: HttpStatus, name: String): ModuleState {
        return try {
            val response = restTemplate.getForEntity(url, String::class.java)
            if (response.statusCode == httpStatus) {
                SimpleModuleState(ModuleState.STATUS.GOOD, "$name Heathcheck", mapOf(Pair("response", response.body ?: EMPTY)))
            } else {
                SimpleModuleState(ModuleState.STATUS.BAD, "$name Heathcheck", mapOf(Pair("statusCode", response.statusCode.toString())))
            }
        } catch (restClientException: RestClientException) {
            SimpleModuleState(ModuleState.STATUS.BAD, "$name Heathcheck", mapOf(Pair("exception", restClientException.localizedMessage)))
        }
    }

    private fun isConfigured(entity: Entity): Boolean {
        return entity.configuration[CONFIG_NAME] != null
    }

    override fun state(entity: Entity): List<ModuleState> {
        return if (entity is ApplicationEntity) {
            states.getOrDefault(entity.appId, emptyMap<String, ModuleState>()).map { it.value }
        } else {
            emptyList()
        }
    }

}

@Configuration
class HttpStatusCodeModuleConfig {
    @Autowired lateinit var registry: ModuleRegistry
    @Autowired lateinit var module: HttpStatusCodeModule

    @PostConstruct
    fun register() {
        registry.registerModule(module, EVERY_SECOND)
    }
}

class NoExceptionErrorHandler : ResponseErrorHandler {
    override fun hasError(response: ClientHttpResponse): Boolean {
        val code = response.statusCode
        return code.is4xxClientError || code.is5xxServerError
    }

    override fun handleError(response: ClientHttpResponse) {}
}
