package org.potatohed.starch.objects

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling
import java.util.LinkedList
import java.util.Properties

@Configuration
@ConfigurationProperties
@EnableScheduling
class Applications {
    var applications: List<Application> = LinkedList()
        get

    @Bean
    fun configuredApplications(mapper: ApplicationEntityMapper): List<ApplicationEntity> = applications.map { mapper.convert(it) }
}

@Configuration
@ConfigurationProperties
class Links {
    var links: List<Link> = LinkedList()
        get

    @Bean
    fun configuredLinks(applications: List<ApplicationEntity>,
                        mapper: LinkEntityMapper): List<LinkEntity> = links.flatMap { mapper.convert(it, applications) }
}

@Configuration
@ConfigurationProperties
class Modules {
    var modules: List<Module> = LinkedList()
        get

    @Bean
    fun configResolver() = fun(moduleId: String): Module {
        return modules.find { it.moduleId == moduleId }
                ?: throw ModuleNotFoundException("Cannot find module with id: $moduleId")
    }
}

class ModuleNotFoundException(msg: String) : RuntimeException(msg)

data class Link(
        var origin: String = "",
        var endNodes: List<String> = LinkedList(),
        var directed: Boolean = false,
        var configuration: Properties = Properties())

data class Application(
        var appId: String = "",
        var appName: String = "",
        var labels: List<String> = LinkedList(),
        var configuration: Properties = Properties())

data class Module(
        var moduleId: String = "",
        var configuration: Properties = Properties())
