package org.potatohed.starch.objects.neo4j

import org.potatohed.starch.objects.*
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

val LINKLOGGER = LoggerFactory.getLogger(LinkEntityMapper::class.java)

@Component
class Neo4jLinkEntityMapper : LinkEntityMapper {

    override fun convert(link: Link, applicationEntities: List<ApplicationEntity>): List<LinkEntity> {
        return link.endNodes.map {
            Relationship(
                    origin = applicationEntities.find(link.origin),
                    endNode = applicationEntities.find(it),
                    configuration = link.configuration.toMap())
        }
                .filter(this::complete)
    }

    private fun complete(rel: Relationship): Boolean {
        val isComplete = !(rel.endNode is IncompleteApplicationNode || rel.origin is IncompleteApplicationNode)
        return if (isComplete) {
            true
        } else {
            LINKLOGGER.error("Unable to complete link", rel)
            false
        }
    }
}

@Component
class Neo4jApplicationEntityMapper : ApplicationEntityMapper {

    override fun convert(application: Application): ApplicationEntity {
        return ApplicationNode(
                appId = application.appId,
                name = application.appName,
                labels = application.labels,
                configuration = application.configuration.toMap())
    }
}

private fun Collection<ApplicationEntity>.find(predicate: String): ApplicationEntity {
    return this.find { it.appId == predicate } ?: IncompleteApplicationNode()
}

fun java.util.Properties.toMap(): Map<String, Map<String, Any>> {
    return this.mapKeys { it.key.toString() }
            .map { val keys = it.key.split("."); Pair(keys[0], Pair(keys[1], it.value!!)) }
            .groupBy { it.first }
            .mapValues { it.value.map { it.second }.toMap() }
}