package org.potatohed.starch.objects.neo4j

import org.apache.commons.lang3.StringUtils.EMPTY
import org.neo4j.ogm.annotation.*
import org.neo4j.ogm.annotation.Properties
import org.potatohed.starch.objects.ApplicationEntity
import org.potatohed.starch.objects.LinkEntity
import java.util.*
import kotlin.collections.HashMap

@NodeEntity(label = "application")
class ApplicationNode(
        override var name: String = EMPTY,
        override var appId: String = EMPTY,
        @Labels override var labels: List<String> = LinkedList(),
        @Properties override var configuration: Map<String, Map<String, Any>> = HashMap()) : ApplicationEntity {

    @GraphId
    var id : Long? = null
}

class IncompleteApplicationNode : ApplicationEntity {
    override var name: String = EMPTY
        get() = throw NotImplementedError("IncompleteNode")
    override var appId: String = EMPTY
        get() = throw NotImplementedError("IncompleteNode")
    override var labels: List<String> = emptyList()
        get() = throw NotImplementedError("IncompleteNode")
    override var configuration: Map<String, Map<String, Any>> = emptyMap()
        get() = throw NotImplementedError("IncompleteNode")

}

@RelationshipEntity(type = "READS")
class Relationship(
        override var name: String = EMPTY,
        @StartNode override var origin: ApplicationEntity = IncompleteApplicationNode(),
        @EndNode override var endNode: ApplicationEntity = IncompleteApplicationNode(),
        @Properties override var configuration: Map<String, Map<String, Any>> = HashMap()) : LinkEntity {

    @GraphId
    var id: Long? = null
}